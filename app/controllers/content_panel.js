angular.module("front-work")
	.controller("ContentPanelCtrl",ContentPanelCtrl);

	//ContentPanelCtrl.$inject = ['$scope'];

	function ContentPanelCtrl($scope,$rootScope){

		var vm = this;
		vm.title = "title content";

		$rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
			vm.title = toState.title;
		});

	}