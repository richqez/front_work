angular.module('front-work')
	.factory('OpenSv',OpenSv);

	function OpenSv(){

		var services = {
			getData : getData
		};

		return services;

		function getData(){
			return {"items":[{"id":"1","client":"Test Client","project":"Project XXX","startDate":1444348800,"endDate":1444953600,"requester":"Supranee J.","lem":"Supranee J.","lep":"Supranee J.","modified":943920000000,"pStatus":"target"},{"id":"11","client":"Test Client 2","project":"Project YYY","startDate":1444348800,"endDate":1444867200,"requester":"Chonnarong H.","lem":"Ken Y.","lep":"","modified":943920000000,"pStatus":"target"}],"total":2,"from":1,"to":2};
		}

	}