angular.module('front-work')
	.config(uiRouteConfig);

	function uiRouteConfig($stateProvider,$urlRouterProvider){

		$stateProvider
			.state('dash',{
				title:"DashBoard",
				url: "/dash",
				templateUrl: "views/content.dash.html"
			})
			.state('open',{
				title:"Opps / Engagements",
				url: "/open",
				templateUrl : "views/content.oops-engagements.html",
				controller : "OpenCtrl",
				controllerAs :"open"
			})
			

		$urlRouterProvider.otherwise("/dash");


	}	